package com.lwj.service.impl;

import com.lwj.dao.StudentDao;
import com.lwj.entity.Page;
import com.lwj.entity.Student;
import com.lwj.service.StudentService;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.util.List;
import java.util.logging.Logger;

/**
 * (Student)表服务实现类
 *
 * @author makejava
 * @since 2020-07-16 10:24:15
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Resource
    private StudentDao studentDao;

    public Student queryById(Integer id) {
        return studentDao.queryById(id);
    }

    public List<Student> queryAllByLimit(int offset, int limit) {
        return studentDao.queryAllByLimit(offset,limit);
    }

    public int insert(Student student) {
        return studentDao.insert(student);
    }

    public int update(Student student) {
        return studentDao.update(student);
    }

    public int deleteById(Integer id) {
        return studentDao.deleteById(id);
    }

    public List<Student> findPage(Page page) {
        return studentDao.queryAllByLimit(page.getPageNo(),page.getPagesize());
    }
}