package com.lwj.vo;

/**
 * @ClassName StudentVo
 * @Author lwj
 * @Date 2020/7/21 15:05
 * @Version 1.0
 * 学生类的vo
 **/


public class StudentVo {

    private Integer id;

    private String name;

    private Integer age;

    private Integer gradeid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGradeid() {
        return gradeid;
    }

    public void setGradeid(Integer gradeid) {
        this.gradeid = gradeid;
    }
}
