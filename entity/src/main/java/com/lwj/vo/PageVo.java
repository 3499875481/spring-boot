package com.lwj.vo;

/**
 * @ClassName PageVo
 * @Author lwj
 * @Date 2020/7/21 15:32
 * @Version 1.0
 * 分页类的vo
 **/
public class PageVo {

    private Integer pageNo;
    private Integer pagesize;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPagesize() {
        return pagesize;
    }

    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNo=" + pageNo +
                ", pagesize=" + pagesize +
                '}';
    }
}
