package com.lwj.entity;

/**
 * @Author：lwj
 * @Date:2020/7/17 0017
 */
public class Page {
    private Integer pageNo;
    private Integer pagesize;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPagesize() {
        return pagesize;
    }

    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNo=" + pageNo +
                ", pagesize=" + pagesize +
                '}';
    }
}
