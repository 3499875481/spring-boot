package com.lwj.dto;

/**
 * @ClassName Dto
 * @Author lwj
 * @Date 2020/7/21 15:04
 * @Version 1.0
 **/
public class Dto<T> {
    private  String success;
    private String errorCode;
    private String msg;
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
