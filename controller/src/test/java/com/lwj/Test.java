package com.lwj;

import java.util.ArrayList;
import java.util.Comparator;

import static java.util.Comparator.comparing;

/**
 * @Author：lwj
 * @Date:2020/7/17 0017
 */
public class Test {
    public static void main(String[] args) {


        ArrayList<Apple> inventory=new ArrayList<>();
        Apple apple=new Apple(10,"red");
        Apple apple1=new Apple(1,"red");
        Apple apple2=new Apple(5,"green");
        inventory.add(apple);
        inventory.add(apple1);
        inventory.add(apple2);

        // 1、传递代码，函数式编程
//        inventory.sort(new AppleComparator());
//        System.out.println(inventory);

        // 2、匿名内部类
        inventory.sort(new Comparator<Apple>() {
            @Override
            public int compare(Apple o1, Apple o2) {
                return o1.getWeight() - o2.getWeight();
            }
        });

        // 3、使用Lambda表达式
        inventory.sort((a, b) -> a.getWeight() - b.getWeight());

        /**
         * 逆序排序
         */
        //1、根据重量逆序排序
        inventory.sort(comparing(Apple::getWeight).reversed());
        System.out.println(inventory);



    }
}
