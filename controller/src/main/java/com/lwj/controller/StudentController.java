package com.lwj.controller;

import com.lwj.entity.Page;
import com.lwj.entity.Student;
import com.lwj.service.StudentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Student)表控制层
 *
 * @author makejava
 * @since 2020-07-16 10:19:27
 */
@RestController
@RequestMapping("student")
public class StudentController {
    /**
     * 服务对象
     */
    @Resource
    private StudentService studentService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @PostMapping("selectOne")
    public Student selectOne(Integer id) {

        return this.studentService.queryById(id);
    }

    /**
     * 通过实体类进行学生添加
     * @param student
     * @return
     */
    @PostMapping("insert")
    public String insert(@RequestBody Student student){
        int insert = studentService.insert(student);
        if(insert>0){
            return "添加成功!";
        }
        return "添加失败!";
    }

    /**
     * 通过id删除数据
     * @param id
     * @return
     */
    @PostMapping("del")
    public String delelte(@RequestParam Integer id){
        int delete=studentService.deleteById(id);
        if(delete>0){
            return "删除成功!";
        }
        return "删除失败!";
    }

    /**
     * 修改用户数据
     * @param student
     * @return
     */

    @PostMapping("update")
    public String update(@RequestBody Student student){
        int update = studentService.update(student);
        if(update>0){
            return "修改成功!";
        }
        return "修改失败!";
    }

    @PostMapping("findAllPage")
    public List<Student> findAllPage(@RequestBody Page page){
        List<Student> list=studentService.findPage(page);
        return list;
    }




}